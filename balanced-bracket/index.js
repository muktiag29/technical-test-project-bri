function balancedBracket(strBracket) {
  if (strBracket.length % 2) return "NO";

  const stack = [];
  const openBracket = "({[";
  const closeBracket = ")}]";
  const pairBracket = { "(": ")", "{": "}", "[": "]" };

  for (const bracket of strBracket) {
    if (openBracket.includes(bracket)) {
      stack.push(bracket);
    }

    if (closeBracket.includes(bracket)) {
      const currentOpenBracket = stack.pop();

      if (bracket !== pairBracket[currentOpenBracket]) {
        return "NO";
      }
    }
  }

  return "YES";
}

["{[()]}", "{[(])}", "{(([])[])[]}"].forEach((e) => {
  console.log("Input:", e, "| Output:", balancedBracket(e));
});
