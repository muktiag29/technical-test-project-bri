function isPalindrome(str) {
  const reversedStr = str.split("").reverse().join("");
  return str === reversedStr ? str : -1;
}

function highestPalindrome(strNum, k) {
  const charLength = strNum.length;

  if (charLength < k || charLength / k < 2) {
    return strNum;
  }

  const compareStart = strNum.slice(0, k);
  const compareEnd = strNum.slice(charLength - k, charLength);

  const compareStartNum = +compareStart;
  const compareEndNum = +compareEnd.split("").reverse().join("");
  let compareResult = compareStartNum;

  if (compareStartNum !== compareEndNum) {
    compareStartNum > compareEndNum
      ? (compareResult = compareStartNum)
      : (compareResult = compareEndNum);
  }

  return (
    compareResult.toString() +
    highestPalindrome(strNum.slice(k, charLength - k), k) +
    compareResult.toString().split("").reverse().join("")
  );
}

[
  ["3943", 1],
  ["3943", 2],
  ["3943", 3],
].forEach((e) => {
  console.log("Input:", e, "| Output:", isPalindrome(highestPalindrome(...e)));
});
