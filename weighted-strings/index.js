function weightedStrings(string, queries) {
  const strIndex = " abcdefghijklmnopqrstuvwxyz";

  const strQuery = [];
  let activeString = "";
  let tempIndexQuery = 0;

  for (const str of string) {
    if (str !== activeString) {
      activeString = str;
      tempIndexQuery = 0;
    }

    tempIndexQuery += strIndex.indexOf(str);
    strQuery.push(tempIndexQuery);
  }

  const response = [];
  let tempQueryIndex = 0;
  for (const query of queries) {
    let isNotMatch = true;

    for (let i = tempQueryIndex; i < strQuery.length; i++) {
      if (query === strQuery[i]) {
        response.push("Yes");
        tempIndexQuery = i + 1;
        isNotMatch = false;
      }
    }

    if (isNotMatch) {
      response.push("No");
    }
  }

  return response;
}

[
  ["abbcccd", [1, 3, 9, 8]],
  ["aceg", [1, 3, 5, 7]],
  ["abbcccd", [2, 3, 5, 7, 11, 13]],
].forEach((e) => {
  console.log("Input:", e, "| Output:", weightedStrings(...e));
});
